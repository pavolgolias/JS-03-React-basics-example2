import React from 'react';
import './UserOutput.css';

const userOutput = (props) => {
    return (
        <div>
            <p className="UserOutput">This is first paragraph, what is your name?</p>
            <p>Username: {props.username}</p>
        </div>
    )
};

export default userOutput
