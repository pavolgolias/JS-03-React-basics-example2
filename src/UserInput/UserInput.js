import React from 'react'

const userInput = (props) => {
    const inlineStyle = {
        textColor: 'red',
        border: '5px solid green'
    };

    return (
        <div>
            <input
                style={inlineStyle}
                type="text"
                onChange={props.changed}
                value={props.username}/>
        </div>
    )
};

export default userInput;
